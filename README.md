# Assignment9
This project aims at creating Contact/PhoneBook app that provides the following features:
1) View all contacts
2) Add a new Contact/PhoneBook
3) View Details of a specific contact

# Prerequisites
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.
The styling requires SCSS attributes. The project is built and run using Angular-CLI.

# Built With
Visual Source Code - IDE to maintain project.
Angular-CLI: to generate the components and services as per Angular framework.
Browser - To run the application and view the app

# Run
Steps:
1) Clone the GitHub repo: https://github.com/neu-mis-info6150-spring-2019/assignment-9-shanks2710.git
2) Open Visual Source Code and Select File->Open Folder
3) Navigate to the respective folder and Click Select Folder
4) Go to Terminal Section
5) Run command `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
6) Run command `ng serve` for the dev server.
7) Navigate to `http://localhost:4200/` in case browser window does not open automatically.

# Author
Shashank Sharma, NU-ID: 001415411

# Acknowledgments
Prof. Amuthan Arulraj and all TAs

# Additional Notes: 
## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
 Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
