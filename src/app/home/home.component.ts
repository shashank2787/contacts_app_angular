// Imports
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

// Component Definition
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // Variable Declarations
  contacts: Object;
  selectedContact: Object;
  showDetails: boolean = false;

  // Constructor
  constructor(private data :  DataService) { }

  // Init Method
  ngOnInit() {
    // Fetch all contacts from REST API on page-load
    this.data.getContacts().subscribe(data => {
      this.contacts = data;
      console.log(this.contacts);
    });
  }

  // Method to show modal-window with contact-details
  onSelect(id: string): void {
    this.showDetails = true;
    // Fetch call to REST API to get contact-details based on ID
    this.data.getContact(id).subscribe(data => {
      this.selectedContact = data;
      console.log(this.selectedContact);
    });
  }

  // Close the modal-window
  closeModal() {
    this.showDetails = false;
  }

}
