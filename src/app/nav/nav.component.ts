// Imports
import { Component, OnInit } from '@angular/core';

// Component-Definition
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  appTitle: string = 'Assignment-9';

  // Constructor
  constructor() { }

  // Init method
  ngOnInit() {
  }

}
