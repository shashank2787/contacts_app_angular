// Imports
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DataService } from "../data.service";

// Component-Definition
@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"]
})
export class ContactComponent implements OnInit {

  // Variable-Declaration
  addContactForm: FormGroup;
  submitted: boolean = false;
  success: boolean = false;

  // Constructor
  constructor(private formBuilder: FormBuilder, private data: DataService) {
    this.addContactForm = this.formBuilder.group({
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      phone: ["", [Validators.required, Validators.pattern(/^\d{10}$/)]],
      email: ["", [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]]
    });
  }

  // Method to add-contact using REST API
  onSubmit() {
    this.submitted = true;
    // Check if Form-Validations failed
    if (this.addContactForm.invalid) {
      return;
    }

    // Map Form-Control values to local variables
    let fname = this.addContactForm.get("firstname").value;
    let lname = this.addContactForm.get("lastname").value;
    let phone = this.addContactForm.get("phone").value;
    let email = this.addContactForm.get("email").value;

    // Call to Add-Contact REST-API
    this.data.addContact(fname, lname, phone, email).subscribe(contact => {
      this.success = true;
    });
  }

  // Init Method
  ngOnInit() {}
}
