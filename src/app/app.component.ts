// Imports
import { Component } from '@angular/core';

// Main-Component for the application
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Assignment-9';
}
