// Imports
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Service-Definition
@Injectable({
  providedIn: "root"
})
export class DataService {
  // Constructor
  constructor(private http: HttpClient) {}

  // Method to make REST API call to fetch all contacts
  getContacts() {
    return this.http.get("http://localhost:3000/contacts");
  }

  // Method to make REST API call to add-contact
  addContact(fname: string, lname: string, phone: number, email: string) {
    return this.http.post("http://localhost:3000/contacts", {
      firstname: fname,
      lastname: lname,
      phone: phone,
      email: email
    });
  }

  // Method to make REST API call to fetch details of a contact based on its ID
  getContact(id: string) {
    return this.http.get("http://localhost:3000/contacts/" + id);
  }
}
